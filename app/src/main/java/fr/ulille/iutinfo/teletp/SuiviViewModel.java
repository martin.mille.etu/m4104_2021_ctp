package fr.ulille.iutinfo.teletp;

import android.app.Application;
import android.content.Context;
import android.view.View;
import android.widget.Toast;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class SuiviViewModel extends AndroidViewModel {
    private MutableLiveData<String> liveLocalisation  = new MutableLiveData<String>();
    private MutableLiveData<String> liveUsername = new MutableLiveData<String>();
    private MutableLiveData<Integer> nextQuestion = new MutableLiveData<Integer>();
    private String[] questions ;
    // TODO Q2.a

    public SuiviViewModel(Application application) {
        super(application);
        liveLocalisation = new MutableLiveData<>();
        liveUsername = new MutableLiveData<>();
        this.nextQuestion= new MutableLiveData<Integer>(0);
    }

    public LiveData<String> getLiveUsername() {
        return liveUsername;
    }

    public void setUsername(String username) {
        liveUsername.setValue(username);
        Context context = getApplication().getApplicationContext();
        this.initQuestion(context);
        Toast toast = Toast.makeText(context, "Username : " + username, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getUsername() {
        return liveUsername.getValue();
    }
    public LiveData<String> getLiveLocalisation() {
        return liveLocalisation;
    }

    public void setLocalisation(String localisation) {
        liveLocalisation.setValue(localisation);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Localisation : " + localisation, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getLocalisation() {
        return liveLocalisation.getValue();
    }
    
    // TODO Q2.a
    public String liveNextQuestion(Integer i){
        return getQuestions(i.intValue());
    }

    public void initQuestion(Context context){
        questions = context.getResources().getStringArray(R.array.list_questions);
    }

    public String getQuestions(int position){ return questions[position];}

    public LiveData<Integer> getLiveNextQuestion(){
        return this.nextQuestion;
    }

    public void setNextQuestion(Integer nextQuestion) {
        this.nextQuestion.setValue(nextQuestion);
    }

    public Integer getNextQuestion(){
        return this.nextQuestion.getValue();
    }


}

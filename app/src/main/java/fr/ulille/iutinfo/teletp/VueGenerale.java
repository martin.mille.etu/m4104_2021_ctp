package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import org.w3c.dom.Text;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String DISTANCIEL ;
    public String poste ="";
    public String salle ;
    public String [] salles;
    SuiviViewModel model ;
    // TODO Q2.c

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        salles = getResources().getStringArray(R.array.list_salles);
        DISTANCIEL= getResources().getStringArray(R.array.list_salles)[0];;
        salle= DISTANCIEL;
        // TODO Q1
        // TODO Q2.c
        model= MainActivity.model;
        // TODO Q4
        Spinner spinnerSalles = (Spinner) getActivity().findViewById(R.id.spSalle);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.list_salles, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinnerSalles.setAdapter(adapter);

        Spinner spinnerPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getContext(),
                R.array.list_postes, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinnerSalles.setAdapter(adapter);
        spinnerPoste.setAdapter(adapter2);
        spinnerPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                poste = parent.getItemAtPosition(position).toString();
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerSalles.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                salle = parent.getItemAtPosition(position).toString();
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
        update();
        // TODO Q5.b
        // TODO Q9
    }

    public void update(){
        Spinner spinnerPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
    if(salle.equals("Distanciel")){
        spinnerPoste.setEnabled(false);
        model.setLocalisation("distanciel");
        }
    else {
        spinnerPoste.setEnabled(true);
        model.setLocalisation(salle+" : " + poste);
    }

    }
    // TODO Q5.a
    // TODO Q9
}
package fr.ulille.iutinfo.teletp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SuiviAdapter extends RecyclerView.Adapter<SuiviAdapter.ViewHolder> {
    private final SuiviViewModel model;

    public SuiviAdapter(SuiviViewModel model) {
        this.model = model;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.question_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String questions = model.getQuestions(position);
        holder.setQuestion(questions);
    }

    @Override
    public int getItemCount() {
        return 0;


    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public String question;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
        public String getQuestion(){
            return question;
        }

        public void setQuestion(String question){
            question=question;
        }

    }
    // TODO Q6.a
    // TODO Q7
}
